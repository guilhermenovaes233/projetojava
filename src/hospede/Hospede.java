package hospede;

import java.util.ArrayList;

import hospede.HospedeDAO;

public class Hospede {
	
	private int idHospede;
	private String email;
	private String nome;
	private String dtNascimento ;
	private String cpf;
	
	public Hospede() {
	
	}
	
	public Hospede( String nome, String email,String dtNascimento, String cpf) {
		this.nome =  nome;
		this.email = email;
		this.dtNascimento = dtNascimento;
		this.cpf = cpf;
	}
	
	public Hospede( String email,String dtNascimento, String cpf) {
		this.email		= email;
		this.dtNascimento = dtNascimento;
		this.cpf = cpf;
	}
	
	public boolean save() {
		HospedeDAO hospedeDAO = new HospedeDAO( this );
		return hospedeDAO.save();
	}
	
	public void delete() {
		HospedeDAO hospedeDAO = new HospedeDAO( this );
		hospedeDAO.delete(this.getIdHospede());
	}
	
	public void alter() {
		HospedeDAO hospedeDAO = new HospedeDAO( this );
		hospedeDAO.alter(this.getIdHospede());
	}
	
	public ArrayList<Hospede> listAll() {
		HospedeDAO hospedeDAO = new HospedeDAO( this );
		return ( hospedeDAO.listAll());
	}
	
	public ArrayList<Hospede> listByEmail(String email) {
		HospedeDAO hospedeDAO = new HospedeDAO( this );
		return ( hospedeDAO.listByEmail(email));
	}
	
	public boolean checkEmail() {
		HospedeDAO hospedeDAO = new HospedeDAO();
		return ( hospedeDAO.checkEmail(this.getEmail()));	
	}
	
	public boolean checkDtNascimento() {
		HospedeDAO hospedeDAO = new HospedeDAO();
		return ( hospedeDAO.checkDtNascimento(this.getDtNascimento()));	
	}
	
	public boolean checkCPF() {
		HospedeDAO hospedeDAO = new HospedeDAO();
		return ( hospedeDAO.checkEmail(this.getEmail()));
	}	
	
	public String[] toArray() {
		return(
		new String[] { 
				new Integer(  this.getIdHospede() ).toString(), 
				this.getEmail(), 
				this.getDtNascimento(),  
				this.getCPF()
		});
	}
	
	public String toString() {
		return(
				new Integer(  this.getIdHospede() ).toString()+ 
				this.getEmail()+ 
				this.getDtNascimento()+
				this.getCPF()
		);
	}
	
	public int getIdHospede() {
		return idHospede;
	}
	
	public void setIdHospede(int idHospede) {
		this.idHospede = idHospede;
	}

	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getDtNascimento() {
		return dtNascimento;
	}
	
	public void setDtNascimento(String DtNascimento) {
		this.dtNascimento = DtNascimento;
	}
	
	public String getCPF() {
		return cpf;
	}
	
	public void setCPF(String Cpf) {
		this.cpf = Cpf;
	}

	public boolean salvar(String email,String dtNascimento, String cpf, String nome ){
		
		Hospede hospede = new Hospede();
		hospede.email = email;
		hospede.cpf = cpf;
		hospede.dtNascimento = dtNascimento;
		
		
		insert();
		
		return true;		
	}
}
